//! Docker client library

#![feature(custom_attribute, custom_derive, plugin, std_misc, unicode, unsafe_destructor)]
#![plugin(serde_macros)]

extern crate rustc_serialize;

extern crate hyper;
extern crate mime;
extern crate serde;
extern crate url;
extern crate openssl;
#[macro_use] extern crate log;
#[cfg(test)] extern crate env_logger;

pub use common::*;
pub use self::dockerv117::*;
pub use self::env::*;
pub use self::flag_parser::*;

mod common;
mod dockerv117;
mod env;
mod flag_parser;
mod urlencoded;
