//! Module for reading docker configuration from the environment

use std::env::{self, VarError};
use ::common::Error;

/// Extract a bool value from a string in the environment
fn var_bool(val: &str) -> bool {
    match val {
        "no"|"0"|"false" => false,
        _ => true
    }
}

fn var_auth(val: Result<String,VarError>) -> Result<Auth, Error> {
    match val {
        Err(_) => Err(Error::InvalidAuth("".to_string())),
        Ok(s) =>
            match &s[..] {
                "none" => Ok(Auth::Insecure),
                "cert" => Ok(Auth::Cert),
                "identity" => Ok(Auth::Identity),
                _ => Err(Error::InvalidAuth(s))
            }
    }
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum ConnectionSpec {
    Http{url: String,
         auth: Auth,
         tls: bool,
         verify: bool,
         cert_path: Option<String>},
    Unix(String),
}

fn url_string(host: &str, tls: bool) -> String {
    format!("{}:{}", if tls {"https"} else {"http"}, host.to_string())
}


macro_rules! merge_option {
    ($a:expr, $b:expr) => {
        match (&$a, &$b) {
            (&Some(_), &None) => $a,
            _ => $b
        }}
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum Auth {
    Insecure,
    Cert,
    Identity,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Env {
    pub host: Option<String>,
    pub auth: Option<Auth>,
    pub tls: Option<bool>,
    pub tls_verify: Option<bool>,
    pub cert_path: Option<String>,
}

impl Env {
    /// Returns a new Env with the given values.
    pub fn new(host: Option<String>,
               tls: Option<bool>,
               tls_verify: Option<bool>,
               auth: Option<Auth>,
               cert_path: Option<String>) -> Env {
        Env {
            host: host,
            auth: auth,
            tls: tls,
            tls_verify: tls_verify,
            cert_path: cert_path,
        }
    }

    /// Returns a new Env instance with values from the process
    /// environment.
    pub fn var() -> Env {
        Env {
            host: env::var("DOCKER_HOST").ok(),
            auth: var_auth(env::var("DOCKER_AUTH")).ok(),
            tls: env::var("DOCKER_TLS").map(|x| var_bool(&x[..])).ok(),
            tls_verify: env::var("DOCKER_TLS_VERIFY").map(|x| var_bool(&x[..])).ok(),
            cert_path: env::var("DOCKER_CERT_PATH").ok(),
        }
    }

    /// Returns a ConnectionSpec instance with values from self.
    pub fn connection_spec(&self) -> Result<ConnectionSpec, Error> {
        debug!("connection_spec: {:?}", self);
        match self.host {
            None =>
                return Ok(ConnectionSpec::Unix("/var/run/docker.sock".to_string())),
            Some(ref s) => {
                let mut c = s.splitn(2,':');
                match c.next() {
                    Some("unix") =>  match c.next() {
                        Some(s) => Ok(ConnectionSpec::Unix(s.to_string())),
                        _ => Err(Error::InvalidEndpoint)
                    },
                    Some("tcp") =>  match c.next() {
                        Some(s) => {
                            let tls = self.tls
                                .unwrap_or(
                                    self.tls_verify == Some(true) ||
                                    self.auth == Some(Auth::Cert));
                            Ok(ConnectionSpec::Http{
                                url: url_string(s, tls),
                                tls: tls,
                                verify: self.tls_verify.unwrap_or(
                                    tls || self.auth == Some(Auth::Identity)),
                                auth: self.auth.clone().unwrap_or(
                                    if tls {Auth::Cert} else {Auth::Identity}),
                                cert_path: self.cert_path.clone(),
                            })},
                        _ => Err(Error::InvalidEndpoint)
                    },
                    _ => Err(Error::InvalidEndpoint)
                }
            }
        }
    }

    pub fn mergs(a: Env, b: Env) -> Env {
        Env {
            host: merge_option!(a.host, b.host),
            auth: merge_option!(a.auth, b.auth),
            tls: merge_option!(a.tls, b.tls),
            tls_verify: merge_option!(a.tls_verify, b.tls_verify),
            cert_path: merge_option!(a.cert_path, b.cert_path),
        }
    }
}

// pub fn get_env() -> Result<Env, VarError> {
//     Ok(Env {
//         host: env::var("DOCKER_HOST").ok(),
//         auth: var_auth(env::var("DOCKER_AUTH")).ok(),
//         tls: env::var("DOCKER_TLS").map(|x| var_bool(&x[])).ok(),
//         tls_verify: env::var("DOCKER_TLS_VERIFY").map(|x| var_bool(&x[])).ok(),
//         cert_path: env::var("DOCKER_CERT_PATH").ok(),
//     })
// }

// fn merge_option<'a>(a: &'a Option<String>, b: &'a Option<String>) -> &'a Option<String> {
//     match (a,b) {
//         (&Some(_), &None) => a,j
//         _ => b
//     }
// }

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn connection_spec_test() {
        assert_eq!(
            ConnectionSpec::Http {
                url: "https://127.0.0.1:2376".to_string(),
                auth: Auth::Cert,
                tls: true,
                verify: true,
                cert_path: Some("/cert/path".to_string())},
            Env::new(Some("tcp://127.0.0.1:2376".to_string()),
                     Some(true),
                     None,
                     None,
                     Some("/cert/path".to_string()))
                .connection_spec().unwrap());
    }
}
