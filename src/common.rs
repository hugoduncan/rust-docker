//! Functions for the docker API that are common across API versions

use std::convert::From;
use std::error;
use std::fmt::{self, Display};
use std::io::{self, Read};
use std::path::PathBuf;
use std::string::FromUtf8Error ;
use serde::{json, Serialize};

use openssl::ssl::{self, SslContext};
use openssl::ssl::error::SslError;
use openssl::x509::X509FileType;

use hyper::{header, Client};
use hyper::net::ContextVerifier;
use mime::{Mime, TopLevel, SubLevel};

use ::env::ConnectionSpec;

// Some basic types that are used across API versions:

/// Container Id
pub type ContainerId = String;

/// Image Id
pub type ImageId = String;

#[derive(Debug)]
pub enum Error {
    HttpError(::hyper::HttpError),
    Error(io::Error),
    JsonError(json::error::Error),
    DecodeError(FromUtf8Error),
    InvalidAuth(String),
    InvalidEndpoint,
}

impl From<::hyper::HttpError> for Error {
    fn from(err: ::hyper::HttpError) -> Error {
        Error::HttpError(err)
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::Error(err)
    }
}

impl From<json::error::Error> for Error {
    fn from(err: json::error::Error) -> Error {
        Error::JsonError(err)
    }
}

impl From<FromUtf8Error> for Error {
    fn from(err: FromUtf8Error) -> Error {
        Error::DecodeError(err)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            Error::HttpError(ref e) => e.fmt(f),
            Error::Error(ref e) => e.fmt(f),
            Error::JsonError(ref e) => e.fmt(f),
            Error::DecodeError(ref e) => e.fmt(f),
            Error::InvalidAuth(ref e) => e.fmt(f),
            Error::InvalidEndpoint => "InvalidEndpoint".fmt(f),
        }
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        "Docker error"
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::HttpError(ref e) => Some(e),
            Error::Error(ref e) => Some(e),
            Error::JsonError(ref e) => Some(e),
            Error::DecodeError(ref e) => Some(e),
            Error::InvalidAuth(_) => None,
            Error::InvalidEndpoint => None,
        }
    }
}

pub type DockerResult<T> = Result<T, Error>;

// Connection to docker endpoint

/// A connection to docker
pub struct Connection {
    pub client: Client,
    pub spec: ConnectionSpec,
}

impl fmt::Debug for Connection {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{:?}", self.spec)
    }
}

impl Connection {
    /// Return a new Connection for the given client and URL string
    pub fn new(client: Client, spec: ConnectionSpec) -> Connection
    {
        let mut c = Connection {
            client: client,
            spec: spec,
        };
        let verifier = c.context_verifier();
        c.client.set_ssl_verifier(verifier);
        c
    }

    // fn set_ssl_verifier<'a: 'b>(&'b mut self)
    // {
    //     self.client.set_ssl_verifier(context_verifier(&self.spec))
    // }

    fn context_verifier(&self) -> ContextVerifier {
        debug!("context_verifier: {:?}", self.spec);
        match self.spec {
            ConnectionSpec::Http{verify: false, ..} =>
                Box::new(|ctxt: &mut SslContext| {
                    ctxt.set_verify(
                        ssl::SSL_VERIFY_PEER |
                        ssl::SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
                        None);
                }),
            ConnectionSpec::Http{verify: true, cert_path: Some(ref p), ..} => {
                let ca_path = PathBuf::from(&format!("{}/ca.pem",p)[..]);
                let cert_path = PathBuf::from(&format!("{}/cert.pem",p)[..]);
                let key_path = PathBuf::from(&format!("{}/key.pem",p)[..]);
                debug!("Cert Paths: {:?}, {:?}, {:?}",
                       ca_path, cert_path, key_path);
                Box::new(move |ctxt: &mut SslContext| {
                    debug!("context_verifier: verify {:?}", ctxt);
                    ctxt.set_verify(
                        ssl::SSL_VERIFY_PEER |
                        ssl::SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
                        None);
                    verify_ssl_error(ctxt.set_CA_file(&ca_path));
                    verify_ssl_error(
                        ctxt.set_certificate_file(&ca_path, X509FileType::PEM));
                    verify_ssl_error(
                        ctxt.set_certificate_file(&cert_path, X509FileType::PEM));
                    verify_ssl_error(
                        ctxt.set_private_key_file(&key_path, X509FileType::PEM));
                })},
            ConnectionSpec::Http{verify: true, ..} =>
                Box::new(|ctxt: &mut SslContext| {
                    ctxt.set_verify(
                        ssl::SSL_VERIFY_PEER |
                        ssl::SSL_VERIFY_FAIL_IF_NO_PEER_CERT,
                        None);
                }),
            _ => panic!("context_verifier on unix connection")
        }
    }
}

impl Connection {
    /// base url
    pub fn base(&self) -> &str {
        match self.spec {
            ConnectionSpec::Http{ref url, ..} => &url[..],
            _ => {
                assert!(false, "Unix sockets not yet supported");
                ""
            }
        }
    }
}

fn verify_ssl_error(res: Result<(), SslError>) {
    if let Err(err) = res {
        error!("SSL error: {}", err);
    }
}

/// Return a URL for an api endpoint
pub fn api_url<Options>(connection: &Connection,
                        root: &str,
                        options: &Options) -> DockerResult<String>
    where Options: Serialize
{
    Ok(format!("{}{}?{}",
               connection.base(),
               root,
               try!(try!(::urlencoded::to_string(options)))))
}

pub fn api_url0<'a>(connection: &'a Connection, root: &str) -> String
{
    format!("{}{}", connection.base(), root)
}

/// API calls common across all versions of the docker API.
pub struct CommonApi;

/// API version information.
#[derive(Debug, PartialEq, Deserialize)]
pub struct ApiVersion {
    #[serde(rename="ApiVersion")]
    api_version: String,
    #[serde(rename="Arch")]
    arch: String,
    #[serde(rename="GitCommit")]
    git_commit: String,
    #[serde(rename="GoVersion")]
    go_version: String,
    #[serde(rename="KernelVersion")]
    kernel_version: String,
    #[serde(rename="Version")]
    version: String,
    #[serde(rename="Os")]
    os: String,
}

impl CommonApi {
    /// Docker API version
    pub fn version(connection: &mut Connection) -> DockerResult<ApiVersion>
    {
        let path = api_url0(&connection, "/version");
        let mut res =
            try!(connection.client
                 .get(&path[..])
                 .header(header::Accept(
                     vec![header::qitem(
                         Mime(TopLevel::Application, SubLevel::Json, vec![]))]))
                 .send());

        let mut s = String::new();
        try!(res.read_to_string(&mut s));
        println!("version json {:?}", s);
        Ok(try!(json::from_str(&s[..])))
    }

}

#[cfg(test)]
mod test {
    use hyper::Client;
    use super::*;
    use ::env::*;

    #[test]
    fn version_test() {
        let co = Env::var().connection_spec().unwrap();
        let client = Client::new();
        // set_ssl_verifier(&mut client, &co);
        println!("Env {:?}", Env::var());
        println!("ConnectionSpec {:?}", co);
        let mut connection = Connection::new(client, co);

        let version = CommonApi::version(&mut connection);
        println!("version {:?}", version);
        assert!(version.is_ok());
    }
}
