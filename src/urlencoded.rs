//! Serialisation support for url encoded structs

use std::f64;
use std::fmt;
use std::num::{Float, FpCategory};
use std::io::{self, Write};
use std::string::FromUtf8Error;

use serde::de::{Deserializer};
use serde::ser::{self, Serialize};

use url::percent_encoding::{percent_encode, FORM_URLENCODED_ENCODE_SET};


// /// A structure for implementing serialization to urlencoded string.
// pub struct Writer<W> {
//     writer: W,
// }

// impl<W: io::Write> Writer<W> {
// /// Creates a Urlencoded visitor whose output will be written to the
// /// specified writer.
// #[inline]
//     pub fn new(writer: W) -> Writer<W> {
//         Writer {
//             writer: writer,
//         }
//     }

//     // /// Unwrap the Writer from the Serializer.
//     // #[inline]
//     // pub fn into_inner(self) -> W {
//     //     self.writer
//     // }
// }

const NONE_MSG : &'static str = "Outputting a None";

struct Serializer<'a, W: 'a + Write> {
    writer: &'a mut W,
    key: Option<String>,
    first: bool,
    elided_first: bool
}

impl<'a, W> fmt::Debug for Serializer<'a, W> where W: 'a + Write {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        try!(write!(f, "Serializer {{ ",));
        try!(write!(f, "key: {:?}, ", self.key));
        try!(write!(f, "first: {:?}, ", self.first));
        try!(write!(f, "elided_first: {:?} ", self.elided_first));
        write!(f, "}}")
    }
}

impl<'a, W: Write> Serializer<'a, W> {
    fn new(w: &mut W) -> Serializer<W> {
        trace!("Serializer::new");
        Serializer {
            writer: w,
            key: None,
            first: false,
            elided_first: false
        }
    }
}

impl<'a, W: Write> ser::Serializer for Serializer<'a, W> {
    type Error = io::Error;

    #[inline]
    fn visit_unit(&mut self) -> Result<(), io::Error> {
        try!(self.writer.write("null".as_bytes()));
        Ok(())
    }

    #[inline]
    fn visit_bool(&mut self, value: bool) -> Result<(), io::Error> {
        if value {
            try!(self.writer.write("true".as_bytes()));
        } else {
            try!(self.writer.write("false".as_bytes()));
        }
        Ok(())
    }

    #[inline]
    fn visit_isize(&mut self, value: isize) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_i8(&mut self, value: i8) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_i16(&mut self, value: i16) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_i32(&mut self, value: i32) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_i64(&mut self, value: i64) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_usize(&mut self, value: usize) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_u8(&mut self, value: u8) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_u16(&mut self, value: u16) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_u32(&mut self, value: u32) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_u64(&mut self, value: u64) -> Result<(), io::Error> {
        write!(self.writer, "{}", value)
    }

    #[inline]
    fn visit_f64(&mut self, value: f64) -> Result<(), io::Error> {
        fmt_f64_or_null(self.writer, value)
    }

    #[inline]
    fn visit_char(&mut self, v: char) -> Result<(), io::Error> {
        escape_char(self.writer, v)
    }

    #[inline]
    fn visit_str(&mut self, value: &str) -> Result<(), io::Error> {
        escape_str(self.writer, value)
    }

    #[inline]
    fn visit_none(&mut self) -> Result<(), io::Error> {
        Err(io::Error::new(::std::io::ErrorKind::Other, NONE_MSG))
    }

    #[inline]
    fn visit_some<V>(&mut self, value: V) -> Result<(), io::Error>
        where V: ser::Serialize
    {
        value.serialize(self)
    }

    #[inline]
    fn visit_seq<V>(&mut self, mut visitor: V) -> Result<(), io::Error>
        where V: ser::SeqVisitor,
    {
        trace!("visit_seq {:?}", self);
        self.first = true;
        while let Some(()) = try!(visitor.visit(self)) { }
        Ok(())
    }

    #[inline]
    fn visit_seq_elt<T>(&mut self, value: T) -> Result<(), io::Error>
        where T: ser::Serialize,
    {
        trace!("visit_seq_elt {:?}", self);
        let first = self.first;
        self.first = false;

        if !first {
            try!(self.writer.write("&".as_bytes()));
            try!(self.writer.write_fmt(
                format_args!("{}",
                             self.key.as_ref()
                             .map(|s| &s[..])
                             .unwrap_or("Nokey"))));
            try!(self.writer.write("=".as_bytes()));
        }

        value.serialize(self)
    }

    #[inline]
    fn visit_map<V>(&mut self, mut visitor: V) -> Result<(), io::Error>
        where V: ser::MapVisitor,
    {
        trace!("visit_map {:?}", self);
        self.first = true;
        while let Some(()) = try!(visitor.visit(self)) { }
        Ok(())
    }

    #[inline]
    fn visit_map_elt<K, V>(&mut self, key: K, value: V) -> Result<(), io::Error>
        where K: ser::Serialize,
              V: ser::Serialize,
    {
        trace!("visit_map_elt {:?}", self);
        if self.first {
            self.elided_first = false;
        }

        if !self.first && !self.elided_first {
            try!(self.writer.write("&".as_bytes()));
        }

        let first = self.first;
        self.first = false;

        let k = try!(to_string(&key));
        trace!("visit_map_elt {:?}", k);
        self.key = Some(k.unwrap());
        trace!("visit_map_elt with key {:?}", self);

        match to_string(&value) {
            Ok(Ok(_)) => {
                trace!("visit_map_elt case 1");
                self.elided_first = false;
                try!(key.serialize(self));
                try!(self.writer.write("=".as_bytes()));
                trace!("visit_map_elt serialize value");
                value.serialize(self)
            },
            Err(ref e) if e.kind() == io::ErrorKind::Other => {
                trace!("visit_map_elt case 2");
                if first {
                    self.elided_first = true;
                }
                Ok(())
            },
            Err(x) => Err(x),
            Ok(Err(_)) => {
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    "Utf8Error"))}
        }
    }
}

fn fmt_f64_or_null<W: Write>(wr: &mut W, value: f64) -> Result<(), io::Error> {
    match value.classify() {
        FpCategory::Nan | FpCategory::Infinite => try!(wr.write("null".as_bytes())),
        _ => try!(wr.write(&f64::to_str_digits(value, 6).as_bytes())),
    };
    Ok(())
}

#[inline]
pub fn escape_str<W: Write>(wr: &mut W, value: &str) -> Result<(), io::Error> {
    try!(wr.write(&percent_encode(value.as_bytes(),
                                  FORM_URLENCODED_ENCODE_SET).as_bytes()));
    Ok(())
}

#[inline]
pub fn escape_char<W: Write>(wr: &mut W, value: char) -> Result<(), io::Error> {
    let mut buf = &mut [0; 4];
    value.encode_utf8(buf);
    try!(wr.write(&percent_encode(buf, FORM_URLENCODED_ENCODE_SET).as_bytes()));
    Ok(())
}

#[inline]
pub fn to_writer<W, T>(wr: &mut W, value: &T) -> Result<(), io::Error>
    where W: Write,
          T: ser::Serialize,
{
    let mut ser = Serializer::new(wr.by_ref());
    try!(value.serialize(&mut ser));
    Ok(())
}

#[inline]
pub fn to_vec<T>(value: &T) -> Result<Vec<u8>, io::Error>
    where T: ser::Serialize,
{
    let mut wr = Vec::with_capacity(128);
    match to_writer(&mut wr, value) {
        Ok(_) => Ok(wr),
        Err(x) => Err(x)
    }
}

#[inline]
pub fn to_string<T>(value: &T) -> Result<Result<String, FromUtf8Error>, io::Error>
    where T: ser::Serialize,
{
    let vec = try!(to_vec(value));
    Ok(String::from_utf8(vec))
}

#[cfg(test)]
mod test {
    use super::*;

    #[derive(Serialize, Deserialize)]
    struct Data {
        foo: Vec<String>,
        v: i64
    }

    #[test]
    fn test_form_urlencoded() {
        let data1 = Data {
            foo: vec!["é&".to_string(),"".to_string(), "#".to_string ()],
            v: 1
        };
        let encoded = to_string(&data1).unwrap().unwrap();
        assert_eq!(encoded, "foo=%C3%A9%26&foo=&foo=%23&v=1".to_string());
        // assert_eq!(parse(encoded.as_bytes()), pairs.as_slice().to_vec());
    }

    #[derive(Serialize, Deserialize)]
    struct OptionData {
        foo: Option<String>,
        v: Option<i64>
    }

    #[test]
    fn test_form_urlencoded_option() {
        let data1 = OptionData {
            foo: None,
            v: Some(1)
        };
        let encoded = to_string(&data1).unwrap().unwrap();
        assert_eq!(encoded, "v=1".to_string());
        // assert_eq!(parse(encoded.as_bytes()), pairs.as_slice().to_vec());
    }
}
