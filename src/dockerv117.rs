/*! Docker API wrapper
*/
use std::default::Default;
use std::collections::HashMap;
use std::io::{BufRead, BufReader, Read};

use serde::json;

use hyper::header;
use hyper::status::StatusCode;
use mime::{Mime, TopLevel, SubLevel};

use ::common::*;

/// The actual API
#[derive(Clone,Copy)]
pub struct Api;

#[derive(Debug, Default, Serialize)]
pub struct ContainersOptions {
    pub all: Option<bool>,
    pub limit: Option<u32>,
    pub since: Option<ContainerId>,
    pub before: Option<ContainerId>,
    pub size: Option<bool>,
    pub filters: Option<HashMap<String,String>>,
}

/// Options when starting a container
#[derive(Debug, Default, Serialize)]
pub struct ContainerCreateOptions {
    /// The name for the container
    pub name: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Port {
    #[serde(rename="PrivatePort")]
    private_port: u16,
    #[serde(rename="Type")]
    protocol: String,
    #[serde(default, rename="Ip")]
    ip: Option<String>,
    #[serde(default, rename="PublicPort")]
    public_port: Option<u16>,
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ContainerInfo{
    #[serde(rename="Id")]
    id: ContainerId,
    #[serde(rename="Image")]
    image: String,
    #[serde(rename="Command")]
    command: String,
    #[serde(rename="Created")]
    created: u64,
    #[serde(rename="Status")]
    status: String,
    #[serde(rename="Ports")]
    ports: Vec<Port>,
    #[serde(rename="Names")]
    names: Vec<String>,
}


#[derive(Debug, PartialEq, Deserialize)]
pub enum Cmd{
    Args(Vec<String>),
    Arg0(String)
}

impl Default for Cmd {
    fn default() -> Cmd {
        Cmd::Arg0("".to_string())
    }
}

impl ::serde::ser::Serialize for Cmd {
    fn serialize<S>(&self, serializer: &mut S) -> Result<(), S::Error>
        where S: ::serde::ser::Serializer
    {
        match self {
            &Cmd::Args(ref v) => v.serialize(serializer),
            &Cmd::Arg0(ref s) => s.serialize(serializer),
        }
    }
}

pub type Empty = HashMap<String, String>;

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct ContainerCreate {
    #[serde(rename="Hostname")]
    pub hostname: Option<String>,
    #[serde(rename="Domainname")]
    pub domainname: Option<String>,
    #[serde(rename="User")]
    pub user: Option<String>,
    #[serde(rename="Memory")]
    pub memory: Option<String>,
    #[serde(rename="MemorySwap")]
    pub memory_swap: Option<String>,
    #[serde(rename="CpuShares")]
    pub cpu_shares: Option<u32>,
    #[serde(rename="Cpuset")]
    pub cpuset: Option<String>,
    #[serde(rename="AttachStdin")]
    pub attach_stdin: bool,
    #[serde(rename="AttachStdout")]
    pub attach_stdout: bool,
    #[serde(rename="AttachStderr")]
    pub attach_stderr: bool,
    #[serde(rename="Tty")]
    pub tty: bool,
    #[serde(rename="OpenStdin")]
    pub open_stdin: bool,
    #[serde(rename="StdinOnce")]
    pub stdin_once: bool,
    #[serde(rename="Env")]
    pub env: Vec<String>,
    #[serde(rename="Cmd")]
    pub cmd: Option<Cmd>,
    #[serde(rename="Entrypoint")]
    pub entrypoint: Option<String>,
    #[serde(rename="Image")]
    pub image: String,
    #[serde(rename="Volumes")]
    pub volumes: HashMap<String,Empty>,
    #[serde(rename="WorkingDir")]
    pub working_dir: Option<String>,
    #[serde(rename="NetworkDisabled")]
    pub network_disabled: bool,
    #[serde(rename="ExposedPorts")]
    pub exposed_ports: HashMap<String,Empty>,
    #[serde(rename="SecurityOpts")]
    pub security_opts: Vec<String>,
    #[serde(rename="HostConfig")]
    pub host_config: HostConfig
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct ContainerCreateResult {
    #[serde(rename="Id")]
    pub id: ContainerId,
    #[serde(rename="Warnings")]
    pub warnings: Vec<String>,
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct ContainerStartResult;

#[derive(Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct RestartPolicy {
    #[serde(rename="Name")]
    pub name: String,
    #[serde(rename="MaximumRetryCount")]
    pub maximum_retry_count: Option<u32>,
}

#[derive(Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct HostConfig {
    #[serde(rename="Binds")]
    pub binds: Vec<String>,
    #[serde(rename="Links")]
    pub links: Vec<String>,
    #[serde(rename="LxcConf")]
    pub lxc_conf: HashMap<String, String>,
    #[serde(rename="PortBindings")]
    pub port_bindings: HashMap<String, Vec<HashMap<String, String>>>,
    #[serde(rename="PublishAllPorts")]
    pub publish_all_ports: bool,
    #[serde(rename="Privileged")]
    pub privileged: bool,   //
    #[serde(rename="ReadOnlyRootFs")]
    pub read_only_root_fs: bool,
    #[serde(rename="Dns")]
    pub dns: Vec<String>,
    #[serde(rename="DnsSearch")]
    pub dns_search: Vec<String>,
    #[serde(rename="ExtraHosts")]
    pub extra_hosts: Vec<String>,
    #[serde(rename="VolumesFrom")]
    pub volumes_from: Vec<String>,
    #[serde(rename="CapAdd")]
    pub cap_add: Vec<String>,
    #[serde(rename="CapDrop")]
    pub cap_drop: Vec<String>,
    #[serde(rename="RestartPolicy")]
    pub restart_policy: Option<RestartPolicy>,
    #[serde(rename="NetworkMode")]
    pub network_mode: String,
    #[serde(rename="Devices")]
    pub devices: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct ContainerDetailConfig {
    #[serde(rename="AttachStderr")]
    pub attach_stderr: bool,
    #[serde(rename="AttachStdin")]
    pub attach_stdin: bool,
    #[serde(rename="AttachStdout")]
    pub attach_stdout: bool,
    #[serde(rename="Cmd")]
    pub cmd: Vec<String>,
    #[serde(rename="CpuShares")]
    pub cpu_shares: u32,
    #[serde(rename="Cpuset")]
    pub cpuset: String,
    #[serde(rename="Domainname")]
    pub domainname: String,
    #[serde(rename="Entrypoint")]
    pub entrypoint: Option<String>,
    #[serde(rename="Env")]
    pub env: Vec<String>,
    #[serde(rename="ExposedPorts")]
    pub exposed_ports: Option<String>,
    #[serde(rename="Hostname")]
    pub hostname: String,
    #[serde(rename="Image")]
    pub image: String,
    #[serde(rename="MacAddress")]
    pub mac_address: String,
    #[serde(rename="Memory")]
    pub memory: u32,
    #[serde(rename="MemorySwap")]
    pub memory_swap: u32,
    #[serde(rename="NetworkDisabled")]
    pub network_disabled: bool,
    #[serde(rename="OnBuild")]
    pub on_build: Option<String>,
    #[serde(rename="OpenStdin")]
    pub open_stdin: bool,
    #[serde(rename="PortSpecs")]
    pub port_specs: Option<String>,
    #[serde(rename="StdinOnce")]
    pub stdin_once: bool,
    #[serde(rename="Tty")]
    pub tty: bool,
    #[serde(rename="User")]
    pub user: String,
    #[serde(rename="Volumes")]
    pub volumes: Option<HashMap<String, Empty>>,
    #[serde(rename="WorkingDir")]
    pub working_dir: String
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct ContainerPortBindings;

#[derive(Debug, PartialEq, Deserialize)]
pub struct ContainerPorts;

#[derive(Debug, PartialEq, Deserialize)]
pub struct ContainerDetailHostConfig {
    #[serde(rename="Binds")]
    pub binds: Vec<String>,
    #[serde(rename="CapAdd")]
    pub cap_add: Vec<String>,
    #[serde(rename="CapDrop")]
    pub cap_drop: Vec<String>,
    #[serde(rename="ContainerIDFile")]
    pub container_id_file: String,
    #[serde(rename="Devices")]
    pub devices: Vec<String>,
    #[serde(rename="Dns")]
    pub dns: Option<String>,
    #[serde(rename="DnsSearch")]
    pub dns_search: Option<String>,
    #[serde(rename="ExtraHosts")]
    pub extra_hosts: Option<String>,
    #[serde(rename="IpcMode")]
    pub ipc_mode: String,
    #[serde(rename="Links")]
    pub links: Vec<String>,
    #[serde(rename="LxcConf")]
    pub lxc_conf: Vec<String>,
    #[serde(rename="NetworkMode")]
    pub network_mode: String,
    #[serde(rename="PortBindings")]
    pub port_bindings: ContainerPortBindings,
    #[serde(rename="Privileged")]
    pub privileged: bool,
    #[serde(rename="ReadonlyRootfs")]
    pub readonly_rootfs: bool,
    #[serde(rename="PublishAllPorts")]
    pub publish_all_ports: bool,
    #[serde(rename="RestartPolicy")]
    pub restart_policy: RestartPolicy,
    #[serde(rename="SecurityOpt")]
    pub security_opt: Option<String>,
    #[serde(rename="VolumesFrom")]
    pub volumes_from: Vec<String>
}

#[derive(Debug, PartialEq, Deserialize)]
pub struct NetworkSettings {
    #[serde(rename="Bridge")]
    pub bridge: String,
    #[serde(rename="Gateway")]
    pub gateway: String,
    #[serde(rename="IPAddress")]
    pub ip_address: String,
    #[serde(rename="IPPrefixLen")]
    pub ip_prefix_len: u32,
    #[serde(rename="MacAddress")]
    pub mac_address: String,
    #[serde(rename="PortMapping")]
    pub port_mapping: ContainerPorts,
    #[serde(rename="Ports")]
    pub ports: Vec<u16>
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct ContainerState {
    #[serde(rename="Error")]
    pub error: String,
    #[serde(rename="ExitCode")]
    pub exit_code: u32,
    #[serde(rename="FinishedAt")]
    pub finished_at: String, // timestamp
    #[serde(rename="OOMKilled")]
    pub oom_killed: bool,
    #[serde(rename="Paused")]
    pub paused: bool ,
    #[serde(rename="Pid")]
    pub pid: u32,
    #[serde(rename="Restarting")]
    pub restarting: bool,
    #[serde(rename="Running")]
    pub running: bool,
    #[serde(rename="StartedAt")]
    pub started_at: String // Timestamp
}

#[derive(Debug, Deserialize)]
pub struct ContainerDetail {
    #[serde(rename="AppArmorProfile")]
    pub app_armor_profile: String,
    #[serde(rename="Args")]
    pub args: Vec<String>,
    #[serde(rename="Config")]
    pub config: ContainerDetailConfig,
    #[serde(rename="Created")]
    pub created: String, // Timestamp
    #[serde(rename="Driver")]
    pub driver: String,
    #[serde(rename="ExecDriver")]
    pub exec_driver: String,
    #[serde(rename="ExecIDs")]
    pub exec_ids: Vec<String>,
    #[serde(rename="HostConfig")]
    pub host_config: ContainerDetailHostConfig,
    #[serde(rename="HostnamePath")]
    pub hostname_path: String,
    #[serde(rename="HostsPath")]
    pub hosts_path: String,
    #[serde(rename="Id")]
    pub id: ContainerId,
    #[serde(rename="Image")]
    pub image: ImageId,
    #[serde(rename="MountLabel")]
    pub mount_label: String,
    #[serde(rename="Name")]
    pub name: String,
    #[serde(rename="NetworkSettings")]
    pub network_settings: NetworkSettings,
    #[serde(rename="Path")]
    pub path: String,
    #[serde(rename="ProcessLabel")]
    pub process_label: String,
    #[serde(rename="ResolvConfPath")]
    pub resolv_conf_path: String,
    #[serde(rename="RestartCount")]
    pub restart_count: u32,
    #[serde(rename="State")]
    pub state: ContainerState,
    #[serde(rename="Volumes")]
    pub volumes: HashMap<String, String>,
    #[serde(rename="VolumesRW")]
    pub volumes_rw: HashMap<String, String>
}


#[derive(Debug, Serialize)]
pub struct Build;

#[derive(Debug, Serialize)]
pub struct BuildOptions {
    /// path within the build context to the Dockerfile
    pub dockerfile: String,
    /// repository name (and optionally a tag) to be applied to the resulting image in case of success
    pub t: Option<String>,
    /// git or HTTP/HTTPS URI build source
    pub remote: Option<String>,
    /// suppress verbose build output
    pub q: bool,
    /// do not use the cache when building the image
    pub nocache: bool,
    /// attempt to pull the image even if an older image exists locally
    pub pull: bool,
    /// remove intermediate containers after a successful build (default behavior)
    pub rm: bool,
    /// always remove intermediate containers (includes rm)
    pub forcerm: bool,
}

#[derive(Debug, Deserialize)]
pub struct ErrorDetail {
    message: String,
}

#[derive(Debug, Deserialize)]
pub struct BuildResultElement {
    #[serde(default)]
    stream: Option<String>,
    #[serde(default)]
    status: Option<String>,
    #[serde(default, rename="errorDetail")]
    error_detail: Option<ErrorDetail>,
    #[serde(default)]
    error: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct BuildResult{
    pub messages: Vec<BuildResultElement>
}

impl Api {
    /// List running containers
    pub fn containers(
        connection: &mut Connection,
        options: &ContainersOptions) -> DockerResult<Vec<ContainerInfo>>
    {
        let path = try!(api_url(&connection, "/containers/json", options));
        let mut res =
            try!(connection.client
                 .get(&path[..])
                 .header(header::Accept(
                     vec![header::qitem(
                         Mime(TopLevel::Application, SubLevel::Json, vec![]))]))
                 .send());

        let mut s = String::new();
        try!(res.read_to_string(&mut s));
        // println!("Response {:?}", s);
        let infos = try!(json::from_str(&s[..]));
        // println!("{:?}",try!(
        //     json::to_string(&Port{
        //         private_port: 1,
        //         protocol: "type".to_string(),
        //         ip: Some("ip".to_string()),
        //         public_port: None})));
        Ok(infos)
    }

    /// Create a container
    pub fn container_create(
        connection: &mut Connection,
        config: &ContainerCreate,
        options: &ContainerCreateOptions) -> DockerResult<ContainerCreateResult>
    {
        debug!("container_create: {:?}, {:?}, {:?}",
               connection, config, options);
        let path = try!(api_url(&connection, "/containers/create", options));
        let body = try!(json::to_string(config));
        debug!("container_create: body {:?}", body);
        let mut res =
            try!(connection.client
                 .post(&path[..])
                 .header(header::Accept(
                     vec![header::qitem(
                         Mime(TopLevel::Application, SubLevel::Json, vec![]))]))
                 .header(header::ContentType(
                     Mime(TopLevel::Application, SubLevel::Json, vec![])))
                 .body(&body[..])
                 .send());

        let mut s = String::new();
        try!(res.read_to_string(&mut s));
        debug!("Response {:?}", s);
        Ok(try!(json::from_str(&s[..])))
    }

    /// Start a container
    pub fn container_start(
        connection: &mut Connection,
        id: &ContainerId) -> DockerResult<ContainerStartResult>
    {
        debug!("container_start: {:?}, {:?}", connection, id);
        let path = api_url0(&connection,
                            &format!("/containers/{}/start", id)[..]);
        let mut res =
            try!(connection.client
                 .post(&path[..])
                 .header(header::Accept(
                     vec![header::qitem(
                         Mime(TopLevel::Application, SubLevel::Json, vec![]))]))
                 .header(header::ContentType(
                     Mime(TopLevel::Application, SubLevel::Json, vec![])))
                 .send());

        debug!("Response status {:?}", res.status);
        if res.status == StatusCode::NoContent {
            Ok(ContainerStartResult)
        } else {
            debug!("Reading response");
            let mut s = String::new();
            try!(res.read_to_string(&mut s));
            debug!("Response {:?}", s);
            Ok(try!(json::from_str(&s[..])))
        }
    }

    /// Inspect a container.
    ///
    /// Return low-level information on the container `id`.
    pub fn container(
        connection: &mut Connection,
        id: &ContainerId) -> DockerResult<ContainerDetail>
    {
        let path = api_url0(&connection, &format!("/containers/{}/json", id)[..]);
        let mut res =
            try!(connection.client
                 .get(&path[..])
                 .header(header::Accept(
                     vec![header::qitem(
                         Mime(TopLevel::Application, SubLevel::Json, vec![]))]))
                 .send());

        let mut s = String::new();
        try!(res.read_to_string(&mut s));
        // println!("Response {:?}", s);
        let detail = try!(json::from_str(&s[..]));
        Ok(detail)
    }

    /// Build an image
    pub fn build<'a, R>(
        connection: &mut Connection,
        options: &BuildOptions,
        tarfile: &'a mut R) -> DockerResult<BuildResult>
        where R: Read
    {
        let path = try!(api_url(&connection, "/build", options));
        let res =
            try!(connection.client
                 .post(&path[..])
                 .header(header::Accept(
                     vec![header::qitem(
                         Mime(TopLevel::Application, SubLevel::Json, vec![]))]))
                 .header(header::ContentType(
                     Mime(TopLevel::Application,
                          SubLevel::Ext("tar".to_string()), vec![])))
                 .header(header::ContentEncoding(vec![header::Encoding::Gzip]))
                 .body(tarfile)
                 .send());


        let mut s = String::new();
        let mut msgs = BuildResult{ messages: vec![] };

        let mut buf_res = BufReader::new(res);
        loop {
            match buf_res.read_line(&mut s) {
                Err(err) => return try!(Err(err)),
                Ok(0) => break,
                Ok(_) => {
                    debug!("build: {}", s);
                    msgs.messages.push(try!(json::from_str(&s)));
                    s.clear();
                }
            }
        }
        Ok(msgs)
    }

}

#[cfg(test)]
mod test {
    use std::default::Default;
    use hyper::Client;
    use super::*;
    use ::common::*;
    use ::env::*;

    #[test]
    fn containers_test() {
        let co = Env::var().connection_spec().unwrap();
        let client = Client::new();
        // set_ssl_verifier(&mut client, &co);
        println!("Env {:?}", Env::var());
        println!("ConnectionSpec {:?}", co);
        let mut connection = Connection::new(client, co);

        let containers = Api::containers(
            &mut connection,
            &ContainersOptions { all: Some(true),
                                 .. Default::default()});
        println!("containers {:?}", containers);
        assert!(containers.is_ok());
    }
}
