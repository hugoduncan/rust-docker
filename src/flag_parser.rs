//! Parsers for docker's flags
use std::collections::HashMap;
use std::default::Default;
use std::error;
use std::fmt::{self, Display};
use std::str::FromStr;

use ::dockerv117::*;

/// An Error type for errors that occur while parsing flags
#[derive(Debug)]
pub enum FlagError{
    InvalidLxcConf(String),
    InvalidRestartPolicy(String),
}

impl Display for FlagError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            FlagError::InvalidLxcConf(ref e) => write!(f, "{}", e),
            FlagError::InvalidRestartPolicy(ref e) => write!(f, "{}", e),
        }
    }
}

impl error::Error for FlagError {
    fn description(&self) -> &str {
        match *self {
            FlagError::InvalidLxcConf(_) => "Invalid LXC config flag",
            FlagError::InvalidRestartPolicy(_) => "Invalid restart policy",
        }
    }
}

/// Parse LXC config flags
pub fn lxc_conf(args: &Vec<String>
                ) -> Result<HashMap<String,String>, FlagError>
{
    args.iter()
        .map(|s| {
            let split = s.splitn(2, '=').collect::<Vec<_>>();
            if split.len() != 2 {
                Err(FlagError::InvalidLxcConf(s.clone()))
            } else {
                Ok((String::from(split[0]),
                    String::from(split[1])))
            }
        })
        .collect::<Result<Vec<_>, FlagError>>()
        .map(|elems| {
            let mut m = HashMap::new();
            m.extend(elems);
            m})
}

/// Parse publish ports
pub fn port_bindings(
    args: &Vec<String>
        ) -> Result<HashMap<String, Vec<HashMap<String, String>>>, FlagError>
{
    let cs = args.iter()
        .map(|s| {
            let split = s.splitn(3, ':').collect::<Vec<_>>();
            match split.len() {
                // container port
                1 => (String::from(split[0]), vec![HashMap::new()]),
                // hostPort:containerPort
                2 => {
                    let mut m = HashMap::new();
                    m.insert(String::from("HostPort"),
                             String::from(split[0]));
                    (String::from(split[1]), vec![m])},
                // ip:hostPort:containerPort
                3 => {
                    let mut m = HashMap::new();
                    m.insert(String::from("HostIp"),
                             String::from(split[0]));
                    if split[1] != "" {
                        m.insert(String::from("HostPort"),
                                 String::from(split[1]));
                    }
                    (String::from(split[1]), vec![m])
                }
                _ => panic!("This should be impossible")
            }
        });
    let mut m = HashMap::new();
    m.extend(cs);
    Ok(m)
}

/// Parse a restart policy
pub fn restart_policy(arg: &str) -> Result<RestartPolicy, FlagError>
{
    let split = arg.splitn(2, ':').collect::<Vec<_>>();
    match split[0] {
        "no" => Ok(RestartPolicy { name: String::from("no"),
                                   .. Default::default() }),
        "on-failure" =>
            Ok(RestartPolicy {
                name: String::from("on-failure"),
                maximum_retry_count: if split.len()==2 {
                    Some(match u32::from_str(split[1]) {
                        Ok(n) => n,
                        Err(_) =>
                            return Err(FlagError::InvalidRestartPolicy(
                                String::from(arg)))
                    })
                } else {
                    None
                }}),
        "always" => Ok(RestartPolicy { name: String::from("always"),
                                       .. Default::default()}),
        x => Err(FlagError::InvalidRestartPolicy(String::from(x)))
    }

}

/// Parse volumes
pub fn volumes(args: &Vec<String>) -> Result<HashMap<String,Empty>, FlagError> {
    let cs = args.iter()
        .map(|s| {
            let split = s.splitn(2, ':').collect::<Vec<_>>();
            match split.len() {
                // path
                1 => (String::from(split[0]), Empty::new()),
                // host path : container path
                // host path : container path : ro
                2|3 => (String::from(split[1]), Empty::new()),
                _ => panic!("This should be impossible")
            }
        });
    let mut m = HashMap::new();
    m.extend(cs);
    Ok(m)
}

/// Parse exposed_ports
pub fn exposed_ports(args: &Vec<String>) -> Result<HashMap<String,Empty>, FlagError> {
    let cs = args.iter()
        .map(|s| {
            (s.clone(), Empty::new())
        });
    let mut m = HashMap::new();
    m.extend(cs);
    Ok(m)
}

/// Parse the command
pub fn command(command: Option<String>, args: Vec<String>) -> Option<Cmd> {
    match command {
        Some(s) => {
            let mut v = vec![s];
            v.extend(args.into_iter());
            Some(Cmd::Args(v))
        }
        None => None
    }
}
