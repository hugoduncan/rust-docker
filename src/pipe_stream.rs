//! Implements a pipe between a write and a read

use std::collections::VecDeque;
use std::io::{self, Read, Write};
use std::sync::{Arc, Condvar, Mutex};

/// A PipeStream takes writes and makes them available as reads.
pub struct PipeStream{
    deque: VecDeque<u8>,
    write_mutex: Mutex<bool>,
    condvar: Condvar,
    shutdown: bool,
}

/// The writable end of a pipe stream
pub struct PipeWrite {
    stream: Arc<PipeStream>
}

/// The readable end of a pipe stream
pub struct PipeRead {
    stream: Arc<PipeStream>
}

impl Read for PipeRead {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut count = 0usize;
        let mut guard = self.stream.write_mutex.lock().unwrap();
        let deque : &mut VecDeque<u8> = unsafe {
            ::std::mem::transmute(&self.stream.deque)
        };
        for i in buf.iter_mut() {
            let mut retry = true;
            while retry {
                if self.stream.deque.is_empty() {
                    if self.stream.shutdown {
                        return Ok(count);
                    }
                    *guard = false;
                    while ! *guard { // ignore spurious wake-ups
                        guard = self.stream.condvar.wait(guard).unwrap();
                    }
                } else {
                    *i = deque.pop_front().unwrap();
                    count += 1;
                    retry = false;
                }
            }
        }
        Ok(count)
    }
}

impl Write for PipeWrite {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        assert!(!self.stream.shutdown);
        let mut guard = self.stream.write_mutex.lock().unwrap();
        let mut count = 0usize;
        let deque : &mut VecDeque<_> = unsafe {
            ::std::mem::transmute(&self.stream.deque)
        };
        for i in buf {
            deque.push_back(*i);
            count += 1 ;
        }
        if count > 0 {
            *guard = true;
            self.stream.condvar.notify_one();
        }
        Ok(count)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(()) // TODO wait for an empty deque?, or just return Ok?
    }
}

impl PipeWrite {
    /// Shutdown the pipe, so there will be no further writes.
    pub fn shutdown(&mut self) {
        let shutdown : &mut bool = unsafe {
            ::std::mem::transmute(&self.stream.shutdown)
        };
        let mut guard = self.stream.write_mutex.lock().unwrap();
        *guard = true;
        *shutdown = true;
        self.stream.condvar.notify_one();
    }
}

impl PipeStream {
    /// Return a new instance
    pub fn new() -> (PipeWrite, PipeRead) {
        let stream = Arc::new(PipeStream {
            deque: VecDeque::new(),
            shutdown: false,
            condvar: Condvar::new(),
            write_mutex: Mutex::new(false)
        });
        (PipeWrite { stream: stream.clone() },
         PipeRead { stream: stream })
    }
}

#[cfg(test)]
mod tests {
    use std::io::{Read, Write};
    // use std::sync::Arc;
    use std::thread;
    use std::time::duration::Duration;

    use env_logger;

    use super::*;

    #[test]
    fn test_pipe_stream_single_thread() {
        let (mut pw, mut pr) = PipeStream::new();
        let d = [0u8, 1u8, 2u8, 3u8, 4u8];
        let r = pw.write(&d);
        assert!(r.is_ok());
        assert_eq!(5, r.unwrap());

        let mut e = [0; 2];
        let r = pr.read(&mut e);
        assert!(r.is_ok());
        assert_eq!([0u8, 1u8], e);
        assert_eq!(2, r.unwrap());
    }

    #[test]
    fn test_pipe_stream_multi_thread() {
        let (mut pw, mut pr) = PipeStream::new();
        let t = thread::scoped(move || {
            thread::sleep(Duration::milliseconds(200));
            let d = [0u8, 1u8, 2u8, 3u8, 4u8];
            let r = pw.write(&d);
            assert!(r.is_ok());
            assert_eq!(5, r.unwrap());
            pw.shutdown();
        });


        let mut e = [0; 2];
        let r = pr.read(&mut e);
        assert!(r.is_ok());
        assert_eq!(2, r.unwrap());
        assert_eq!([0u8, 1u8], e);

        let r = pr.read(&mut e);
        assert!(r.is_ok());
        assert_eq!([2u8, 3u8], e);
        assert_eq!(2, r.unwrap());

        let r = pr.read(&mut e);
        assert!(r.is_ok());
        assert_eq!([4u8, 3u8], e);
        assert_eq!(1, r.unwrap());

        let r = pr.read(&mut e);
        assert!(r.is_ok());
        assert_eq!(0, r.unwrap());

        t.join();
    }

    #[test]
    fn test_pipe_stream_shutdown() {
        let (mut pw, mut pr) = PipeStream::new();

        let t = thread::scoped(move || {
            let mut e = [0; 3];
            let r = pr.read(&mut e);
            assert!(r.is_ok());
            assert_eq!([0u8, 1u8, 0u8], e);
            assert_eq!(2, r.unwrap());
        });

        let d = [0u8, 1u8];
        let r = pw.write(&d);
        assert!(r.is_ok());
        assert_eq!(2, r.unwrap());
        pw.shutdown();

        t.join();
    }

}
